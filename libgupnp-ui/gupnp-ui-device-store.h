/* 
 * Copyright (C) 2007 OpenedHand Ltd.
 *
 * Author: Jorn Baayen <jorn@openedhand.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GUPNP_UI_DEVICE_STORE_H__
#define __GUPNP_UI_DEVICE_STORE_H__

#include <libgupnp/gupnp-control-point.h>
#include <gtk/gtkliststore.h>

G_BEGIN_DECLS

/**
 * GUPnPUIDeviceStoreCol
 *
 * #GtkTreeModel column numbers.
 *
 * @GUPNP_UI_DEVICE_STORE_COL_PROXY: Column of type #GUPNP_TYPE_DEVICE_PROXY
 * containing the device's #GUPnPDeviceProxy.
 * @GUPNP_UI_DEVICE_STORE_COL_FRIENDLY_NAME: Column of type #G_TYPE_STRING
 * containing the device's friendly name.
 **/
typedef enum {
        GUPNP_UI_DEVICE_STORE_COL_PROXY         = 0,
        GUPNP_UI_DEVICE_STORE_COL_FRIENDLY_NAME = 1
} GUPnPUIDeviceStoreCol;

GType
gupnp_ui_device_store_get_type (void) G_GNUC_CONST;

#define GUPNP_UI_TYPE_DEVICE_STORE \
                (gupnp_ui_device_store_get_type ())
#define GUPNP_UI_DEVICE_STORE(obj) \
                (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
                 GUPNP_UI_TYPE_DEVICE_STORE, \
                 GUPnPUIDeviceStore))
#define GUPNP_UI_DEVICE_STORE_CLASS(obj) \
                (G_TYPE_CHECK_CLASS_CAST ((obj), \
                 GUPNP_UI_TYPE_DEVICE_STORE, \
                 GUPnPUIDeviceStoreClass))
#define GUPNP_UI_IS_DEVICE_STORE(obj) \
                (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
                 GUPNP_UI_TYPE_DEVICE_STORE))
#define GUPNP_UI_IS_DEVICE_STORE_CLASS(obj) \
                (G_TYPE_CHECK_CLASS_TYPE ((obj), \
                 GUPNP_UI_TYPE_DEVICE_STORE))
#define GUPNP_UI_DEVICE_STORE_GET_CLASS(obj) \
                (G_TYPE_INSTANCE_GET_CLASS ((obj), \
                 GUPNP_UI_TYPE_DEVICE_STORE, \
                 GUPnPUIDeviceStoreClass))

typedef struct _GUPnPUIDeviceStorePrivate GUPnPUIDeviceStorePrivate;

typedef struct {
        GtkListStore parent;

        GUPnPUIDeviceStorePrivate *priv;
} GUPnPUIDeviceStore;

typedef struct {
        GtkListStoreClass parent_class;

        /* future padding */
        void (* _gupnp_ui_reserved1) (void);
        void (* _gupnp_ui_reserved2) (void);
        void (* _gupnp_ui_reserved3) (void);
        void (* _gupnp_ui_reserved4) (void);
} GUPnPUIDeviceStoreClass;

GUPnPUIDeviceStore *
gupnp_ui_device_store_new               (GUPnPControlPoint  *control_point);

GUPnPControlPoint *
gupnp_ui_device_store_get_control_point (GUPnPUIDeviceStore *store);

G_END_DECLS

#endif /* __GUPNP_UI_DEVICE_STORE_H__ */
