/* 
 * Copyright (C) 2007 OpenedHand Ltd.
 *
 * Author: Jorn Baayen <jorn@openedhand.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**
 * SECTION:gupnp-ui-device-view
 * @short_description: #GtkTreeView exposing a #GUPnPUIDeviceStore.
 *
 * #GUPnPUIDeviceView exposes the contents of a #GUPnPUIDeviceStore as
 * a #GtkTreeView.
 */

#include <config.h>
#include <gtk/gtkcellrenderertext.h>
#include <glib/gi18n-lib.h>

#include "gupnp-ui-device-view.h"

G_DEFINE_TYPE (GUPnPUIDeviceView,
               gupnp_ui_device_view,
               GTK_TYPE_TREE_VIEW);

static void
gupnp_ui_device_view_init (GUPnPUIDeviceView *view)
{
        GtkTreeView *tree_view;

        tree_view = GTK_TREE_VIEW (view);

        gtk_tree_view_set_headers_visible (tree_view, FALSE);

        gtk_tree_view_insert_column_with_attributes
                (tree_view,
                 -1,
                 _("Name"),
                 gtk_cell_renderer_text_new (),
                 "text",
                 GUPNP_UI_DEVICE_STORE_COL_FRIENDLY_NAME,
                 NULL);
}

static void
gupnp_ui_device_view_class_init (GUPnPUIDeviceViewClass *klass)
{
}

/**
 * gupnp_ui_device_view_new
 * @store: The #GUPnPUIDeviceStore to expose
 *
 * Return value: A new #GUPnPUIDeviceView object.
 **/
GtkWidget *
gupnp_ui_device_view_new (GUPnPUIDeviceStore *store)
{
        g_return_val_if_fail (GUPNP_UI_IS_DEVICE_STORE (store), NULL);

        return g_object_new (GUPNP_UI_TYPE_DEVICE_VIEW,
                             "model", store,
                             NULL);
}
