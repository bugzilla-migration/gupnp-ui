/* 
 * Copyright (C) 2007 OpenedHand Ltd.
 *
 * Author: Jorn Baayen <jorn@openedhand.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**
 * SECTION:gupnp-ui-device-store
 * @short_description: #GtkTreeModel exposing a #GUPnPControlPoint.
 *
 * #GUPnPUIDeviceStore exposes the discovery results of a #GUPnPControlPoint
 * through a #GtkTreeModel interface.
 */

#include "gupnp-ui-device-store.h"

G_DEFINE_TYPE (GUPnPUIDeviceStore,
               gupnp_ui_device_store,
               GTK_TYPE_LIST_STORE);

struct _GUPnPUIDeviceStorePrivate {
        GUPnPControlPoint *control_point;

        guint              device_available_id;
        guint              device_unavailable_id;
};

enum {
        PROP_0,
        PROP_CONTROL_POINT
};

static void
gupnp_ui_device_store_set_control_point (GUPnPUIDeviceStore *store,
                                         GUPnPControlPoint  *control_point);

static void
gupnp_ui_device_store_set_property (GObject      *object,
                                    guint         property_id,
                                    const GValue *value,
                                    GParamSpec   *pspec)
{
        GUPnPUIDeviceStore *store;

        store = GUPNP_UI_DEVICE_STORE (object);

        switch (property_id) {
        case PROP_CONTROL_POINT:
                gupnp_ui_device_store_set_control_point
                                        (store, g_value_get_object (value));
                break;
        default:
                G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
                break;
        }
}

static void
gupnp_ui_device_store_get_property (GObject    *object,
                                    guint       property_id,
                                    GValue     *value,
                                    GParamSpec *pspec)
{
        GUPnPUIDeviceStore *store;

        store = GUPNP_UI_DEVICE_STORE (object);

        switch (property_id) {
        case PROP_CONTROL_POINT:
                g_value_set_object (value, store->priv->control_point);
                break;
        default:
                G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
                break;
        }
}

static void
gupnp_ui_device_store_dispose (GObject *object)
{
        GUPnPUIDeviceStore *store;
        GObjectClass *object_class;

        store = GUPNP_UI_DEVICE_STORE (object);

        if (store->priv->control_point) {
                if (g_signal_handler_is_connected
                        (store->priv->control_point,
                         store->priv->device_available_id)) {
                        g_signal_handler_disconnect
                                (store->priv->control_point,
                                 store->priv->device_available_id);
                }

                if (g_signal_handler_is_connected
                        (store->priv->control_point,
                         store->priv->device_unavailable_id)) {
                        g_signal_handler_disconnect
                                (store->priv->control_point,
                                 store->priv->device_unavailable_id);
                }

                g_object_unref (store->priv->control_point);
                store->priv->control_point = NULL;
        }

        /* Call super */
        object_class = G_OBJECT_CLASS (gupnp_ui_device_store_parent_class);
        object_class->dispose (object);
}

/* Sorts by friendly name */
static int
sort_func (GtkTreeModel *tree_model,
           GtkTreeIter  *a,
           GtkTreeIter  *b,
           gpointer      user_data)
{
        char *a_friendly_name, *b_friendly_name;
        char *a_friendly_name_folded, *b_friendly_name_folded;
        int ret;

        gtk_tree_model_get (tree_model,
                            a,
                            GUPNP_UI_DEVICE_STORE_COL_FRIENDLY_NAME,
                            &a_friendly_name,
                            -1);

        gtk_tree_model_get (tree_model,
                            b,
                            GUPNP_UI_DEVICE_STORE_COL_FRIENDLY_NAME,
                            &b_friendly_name,
                            -1);

        /* Compare case insensitively */
        a_friendly_name_folded = g_utf8_casefold (a_friendly_name, -1);
        b_friendly_name_folded = g_utf8_casefold (b_friendly_name, -1);

        g_free (a_friendly_name);
        g_free (b_friendly_name);

        ret = g_utf8_collate (a_friendly_name_folded, b_friendly_name_folded);

        g_free (a_friendly_name_folded);
        g_free (b_friendly_name_folded);

        return ret;
}

static void
gupnp_ui_device_store_init (GUPnPUIDeviceStore *store)
{
        GtkTreeSortable *sortable;
        GType types[2];

        store->priv = G_TYPE_INSTANCE_GET_PRIVATE (store,
                                                   GUPNP_UI_TYPE_DEVICE_STORE,
                                                   GUPnPUIDeviceStorePrivate);

        /* Set up column types */
        types[GUPNP_UI_DEVICE_STORE_COL_PROXY] =
                                                GUPNP_TYPE_DEVICE_PROXY;
        types[GUPNP_UI_DEVICE_STORE_COL_FRIENDLY_NAME] =
                                                G_TYPE_STRING;

        gtk_list_store_set_column_types (GTK_LIST_STORE (store),
                                         2,
                                         types);

        /* Sort on friendly name by default */
        sortable = GTK_TREE_SORTABLE (store);

        gtk_tree_sortable_set_sort_func
                (sortable,
                 GUPNP_UI_DEVICE_STORE_COL_FRIENDLY_NAME,
                 sort_func,
                 NULL,
                 NULL);

        gtk_tree_sortable_set_sort_column_id
                (GTK_TREE_SORTABLE (store),
                 GUPNP_UI_DEVICE_STORE_COL_FRIENDLY_NAME,
                 GTK_SORT_ASCENDING);
}

static void
gupnp_ui_device_store_class_init (GUPnPUIDeviceStoreClass *klass)
{
        GObjectClass *object_class;

        object_class = G_OBJECT_CLASS (klass);

        object_class->set_property = gupnp_ui_device_store_set_property;
        object_class->get_property = gupnp_ui_device_store_get_property;
        object_class->dispose      = gupnp_ui_device_store_dispose;

        g_type_class_add_private (klass, sizeof (GUPnPUIDeviceStorePrivate));

        /**
         * GUPnPUIDeviceStore:control-point
         *
         * The exposed #GUPnPControlPoint.
         **/
        g_object_class_install_property
                (object_class,
                 PROP_CONTROL_POINT,
                 g_param_spec_object ("control-point",
                                      "Control point",
                                      "The exposed GUPnPControlPoint",
                                      GUPNP_TYPE_CONTROL_POINT,
                                      G_PARAM_READWRITE |
                                      G_PARAM_CONSTRUCT_ONLY |
                                      G_PARAM_STATIC_NAME |
                                      G_PARAM_STATIC_NICK |
                                      G_PARAM_STATIC_BLURB));
}

/**
 * gupnp_ui_device_store_new
 * @control_point: The #GUPnPControlPoint to expose
 *
 * Return value: A new #GUPnPUIDeviceStore object.
 **/
GUPnPUIDeviceStore *
gupnp_ui_device_store_new (GUPnPControlPoint *control_point)
{
        g_return_val_if_fail (GUPNP_IS_CONTROL_POINT (control_point), NULL);

        return g_object_new (GUPNP_UI_TYPE_DEVICE_STORE,
                             "control-point", control_point,
                             NULL);
}

/* Device available: Insert into store */
static void
device_available_cb (GUPnPControlPoint *control_point,
                     GUPnPDeviceProxy  *proxy,
                     gpointer           user_data)
{
        char *friendly_name;

        friendly_name =
                gupnp_device_info_get_friendly_name (GUPNP_DEVICE_INFO (proxy));

        gtk_list_store_insert_with_values
                (GTK_LIST_STORE (user_data),
                 NULL,
                 0,
                 GUPNP_UI_DEVICE_STORE_COL_PROXY,
                 proxy,
                 GUPNP_UI_DEVICE_STORE_COL_FRIENDLY_NAME,
                 friendly_name,
                 -1);

        g_free (friendly_name);
}

/* Device unavailable: Remove from store */
static void
device_unavailable_cb (GUPnPControlPoint *control_point,
                       GUPnPDeviceProxy  *proxy,
                       gpointer           user_data)
{
        GtkTreeModel *tree_model;
        GUPnPDeviceProxy *row_proxy;
        GtkTreeIter iter;
        gboolean state;

        tree_model = GTK_TREE_MODEL (user_data);

        /* Find matching row */
        state = gtk_tree_model_get_iter_first (tree_model, &iter);
        while (state) {
                gtk_tree_model_get (tree_model,
                                    &iter,
                                    GUPNP_UI_DEVICE_STORE_COL_PROXY,
                                    &row_proxy,
                                    -1);

                g_object_unref (row_proxy);

                if (proxy == row_proxy) {
                        /* Found: Remove */
                        gtk_list_store_remove (GTK_LIST_STORE (user_data),
                                               &iter);

                        break;
                }

                state = gtk_tree_model_iter_next (tree_model, &iter);
        }

}

static void
gupnp_ui_device_store_set_control_point (GUPnPUIDeviceStore *store,
                                         GUPnPControlPoint  *control_point)
{
        GtkListStore *list_store;
        const GList *devices;
        GList *l;

        list_store = GTK_LIST_STORE (store);

        store->priv->control_point = g_object_ref (control_point);

        /* Listen for changes */
        store->priv->device_available_id =
                g_signal_connect_object (control_point,
                                         "device-proxy-available",
                                         G_CALLBACK (device_available_cb),
                                         G_OBJECT (store),
                                         0);

        store->priv->device_unavailable_id =
                g_signal_connect_object (control_point,
                                         "device-proxy-unavailable",
                                         G_CALLBACK (device_unavailable_cb),
                                         G_OBJECT (store),
                                         0);

        /* Insert existing proxies */
        devices = gupnp_control_point_list_device_proxies (control_point);
        for (l = (GList *) devices; l; l = l->next)
                device_available_cb (control_point, l->data, store);
}

/**
 * gupnp_ui_device_store_get_control_point
 * @store: A #GUPnPUIDeviceStore
 *
 * Return value: The exposed #GUPnPControlPoint.
 **/
GUPnPControlPoint *
gupnp_ui_device_store_get_control_point (GUPnPUIDeviceStore *store)
{
        g_return_val_if_fail (GUPNP_UI_IS_DEVICE_STORE (store), NULL);

        return store->priv->control_point;
}
