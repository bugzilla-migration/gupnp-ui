/* 
 * Copyright (C) 2007 OpenedHand Ltd.
 *
 * Author: Jorn Baayen <jorn@openedhand.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GUPNP_UI_DEVICE_VIEW_H__
#define __GUPNP_UI_DEVICE_VIEW_H__

#include <gtk/gtktreeview.h>

#include "gupnp-ui-device-store.h"

G_BEGIN_DECLS

GType
gupnp_ui_device_view_get_type (void) G_GNUC_CONST;

#define GUPNP_UI_TYPE_DEVICE_VIEW \
                (gupnp_ui_device_view_get_type ())
#define GUPNP_UI_DEVICE_VIEW(obj) \
                (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
                 GUPNP_UI_TYPE_DEVICE_VIEW, \
                 GUPnPUIDeviceView))
#define GUPNP_UI_DEVICE_VIEW_CLASS(obj) \
                (G_TYPE_CHECK_CLASS_CAST ((obj), \
                 GUPNP_UI_TYPE_DEVICE_VIEW, \
                 GUPnPUIDeviceViewClass))
#define GUPNP_UI_IS_DEVICE_VIEW(obj) \
                (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
                 GUPNP_UI_TYPE_DEVICE_VIEW))
#define GUPNP_UI_IS_DEVICE_VIEW_CLASS(obj) \
                (G_TYPE_CHECK_CLASS_TYPE ((obj), \
                 GUPNP_UI_TYPE_DEVICE_VIEW))
#define GUPNP_UI_DEVICE_VIEW_GET_CLASS(obj) \
                (G_TYPE_INSTANCE_GET_CLASS ((obj), \
                 GUPNP_UI_TYPE_DEVICE_VIEW, \
                 GUPnPUIDeviceViewClass))

typedef struct {
        GtkTreeView parent;

        /* future padding */
        gpointer _gupnp_ui_reserved;
} GUPnPUIDeviceView;

typedef struct {
        GtkTreeViewClass parent_class;

        /* future padding */
        void (* _gupnp_ui_reserved1) (void);
        void (* _gupnp_ui_reserved2) (void);
        void (* _gupnp_ui_reserved3) (void);
        void (* _gupnp_ui_reserved4) (void);
} GUPnPUIDeviceViewClass;

GtkWidget *
gupnp_ui_device_view_new (GUPnPUIDeviceStore *store);

G_END_DECLS

#endif /* __GUPNP_UI_DEVICE_VIEW_H__ */
